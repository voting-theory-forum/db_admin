# Database Administration #

I tried it from _nodejs_, but it's probably more productive
via the `mongo` shell. Learning two environments, with 
different names for the same commands, is annoying, but on 
the other hand, the tools in the `mongo` shell remove the
need to deal with promises, and probably provide some useful
additional shortcuts.

There are subdirectories here for the two approaches.
