# In the Mongo Shell #
```
mongo

tmp = {}

typeof (tmp.self = tmp)

tmp.conn = Mongo()

with (tmp) self.admin = conn.getDB("admin")

tmp.adm_pw = ...

with (tmp) admin.auth("admin", adm_pw);

with (tmp) self.archive = conn.getDB("archive");

with (tmp) self.r = archive.getUser( "archive", {
   showCredentials: true,
   showPrivileges: true,
   showAuthenticationRestrictions: true
   /* no filter */
} )

```
