# In Nodejs *

_This may be the approach to administration that provides_ 
_less leverage. See the parent and sibling directories._

Have to authenticate as the administrator. My working notion
of the model of that is that every server has an "admin"
database, and it is databases that know users, not the
server as a whole, and that the way to administer the
server is to connect with reference to the admin database
and authenticate as a user it knows, a user having a role to
administer the server. I will test this model and see
whether it holds up.

Once I will have succeeded to connect and authenticate as
a user with the proper roles, then when it comes time to
create a database, maybe all I shall have to do is refer to
the future database by its name and start doing operations.
And indeed that assumption seems to pan out.
```
npm init -y
npm install mongodb

git config user.email m2hh2kmhsn@snkmail.com
git config user.name "Jack WAUGH"

node

typeof {} == typeof tmp || (tmp = {});
tmp.tmp = tmp;
tmp.self = tmp
```
Following is not to put in exactly; it is a distillation
from the config of the staging NodeBB, to remind me how
to reach the MongoDB server.
```
        "host": "127.0.0.1",
        "port": 27017,
        "uri": "mongodb://"
```
Let's see how to make it work.

Manually set `tmp.admin_pw` to the Mongodb admin password.

As the
[Manual](https://docs.mongodb.com/manual/reference/connection-string/)
says,

If the password includes any ofthe following
characters:
```
: / ? # [ ] @
```
those characters must be converted using
[percent encoding](https://tools.ietf.org/html/rfc3986#section-2.1).
I'm surprised the list does not include the ampersand.
```
typeof (tmp.MongoClient = require("mongodb").MongoClient);

tmp.mongo_uri = 
  "mongodb://admin:" + tmp.admin_pw +
  "@127.0.0.1:27017/admin";

typeof ( tmp.mongo_client = new tmp.MongoClient(
  tmp.mongo_uri, {useUnifiedTopology: true}
) );

( async function () { with (tmp) {
  try {
    await mongo_client.connect();
    console.log("Connected.");
  } catch (err) {
    console.error(err)
  }
}})();

typeof (tmp.db = tmp.mongo_client.db("archive"));
```
tpl
```
( async function () { with (tmp) {
  try {
  } catch (err) {
    console.error(err)
  }
}})();
```
End of template; keep going with snippets.

Set tmp.pw to password from vault of secrets, for the 
archive user.
```
( async function () { with (tmp) {
  try { await db.addUser("archive", pw, {
      roles: ['readWrite']
  })} catch (err) {console.error(err)}
}})();
```
I'm not seeing a way to list the users in a database, but
the above ran without throwing any error, so I guess no news
is good news.

(later) Bad news. The user could not use. One or more
things are wrong with my procedures.

Going to try to understand what state I have set up, in
order to be able to even have a start toward correcting it.
```
typeof (tmp.admin_db = tmp.mongo_client.db("admin"));

( async function () { with (tmp) {
  try {
    self.admin_colls = await admin_db.collections();
    console.log("admin_colls");
  } catch (err) {
    console.error(err)
  }
}})();

with (tmp) for (c of admin_colls)
  console.log(c.collectionName);

with (tmp) typeof (self.cur = admin_colls[0].find())

with (tmp) ( self.next = async function () {
  try {
    self.got = await cur.next();
    console.log("got");
  } catch (err) {
    console.error(err)
  }
})();

with (tmp) Object.keys(got)
with (tmp) Object.keys(got.roles)
with (tmp) Object.keys(got.roles[0])
with (tmp) got.roles[0].db
with (tmp) got.roles[0].role
with (tmp) Object.keys(got.user)
with (tmp) got.user
```
The roles look correct, because they correspond with those
of a user that works (nodebb). So it looks as though my 
problem is exactly as the diagnostic said, one of 
authentication.
```
with (tmp) Object.keys(got.credentials)
with (tmp) Object.keys(got.customData)
```
Reset the user password, making double sure to use the one
I intended to use, and save the code snippet that determines
it. My offline notes now say tmp.pw = and a literal, and I
will paste that in.
```
typeof (tmp.db = tmp.mongo_client.db("archive"));

tmp.admin_cmd = {
  updateUser: "archive",
  pwd: "tmp.pw"
}

with (tmp) ( async function () {
  try {
    await db.executeDbAdminCommand(admin_cmd);
    console.log("Submitted command.");
  } catch (err) {
    console.error(err)
  }
})();


```
